var mongoose = require('mongoose'),
    url = 'mongodb://localhost:27017/demoApp',
    _DB;

module.exports = {
    connect : function(){
        mongoose.Promise = global.Promise;
        mongoose.connect(url, function (err, db) {
            if(err) {
                console.log(err);
                process.exit(1);
            }
            console.log('Connected');
            _DB = db;
    })},
    getDb : function(){
        return _DB;
    }
};
