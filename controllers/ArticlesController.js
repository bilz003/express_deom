'use strict';
var viewData = {
	'layout' : {
		'title': 'Effective View Rendering for Articles'
	},
	'bodyFile' : '',                   //Relative to the layout file
	'bodyData' : {
		'message': 'Hey, I am the content part of article :)'
	} 
};
var folder = '../articles/';

module.exports = {
	list: function (req, res) {
		viewData.bodyFile = folder + 'list';
	  	res.render('layout/common', viewData);
	}
}
