'use strict';

var bs = require('./../../config/bootstrap'),
	Admin = require(bs.ROOT + '/models/AdminsModel');

var viewData = {
	'layout' : {
		'title': 'Effective View Rendering for Admin'
	},
	'bodyFile' : '',                   //Relative to the layout file
	'bodyData' : {
		'message': 'Hey, I am the content part of admin :)'
	} 
};
var folder = '../admin/admins/';

module.exports = {
	list: function (req, res) {
		viewData.bodyFile = folder + 'list.html';
	  	res.render('layout/common', viewData);
	},
	add: function (req, res){
		var adminEntity = new Admin.Schema(req.body);
		adminEntity.save(function(err, Admin) {
			if(err) {
				res.send(err);
			} else {
				res.send(Admin)
			}
			//res.send('Added :)');
		});
	},


    findOne: function (req, res){
        var adminEntity = Admin.Schema;
        adminEntity.findOne({'firstname': 'Jambu'}, 'firstname lastname', function(err, result) {
            res.send(result);
        })
    },
    findById: function (req, res){
        var adminEntity = Admin.Schema;
        adminEntity.findById("5c921577d15a92413ee896ad", function(err, admins) {
            res.send(admins);
        })
    },
	find: function (req, res){
		var adminEntity = Admin.Schema;
		//Pass this to populate method, to have multiple associated records
        var populateQuery = [{path:'role_id', select:' ' /*or ['1', '2']*/}, {path:'role_id2', select:'name active'}];
        //For deep association record fetching
        var populateQuery = [{path:'role_id', 'populate': [{path: 'XYZ'}]}];
		adminEntity
			.find()
			.limit()
            .populate('role_id')
            //.where('age').gt(17).lt(20)
            .where({'firstname': 'Jambu', 'age': {$gt: 17, $lt: 20}})
            .where('_id').equals("5c92427e042e8463db39b7f6")    //Having multiple role
        	//.where('_id').equals("5c92421bb2ec2e633ceb6c24")    //single role
			.sort({ _id: 1})
			// .select({'firstname': 1, 'address.street': 1 })
			.exec(function(err, result) {
                res.send(result);
            })
	},
}
