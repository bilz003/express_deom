'use strict';

var bs = require('./../../config/bootstrap'),
	Roles = require(bs.ROOT + '/models/RolesModel');

module.exports = {
	list: function(req, res) {
		Roles.Schema.find(function(err, Roles) {
            res.send(Roles);
        })
	},
	add: function (req, res){
		var roleEntity = new Roles.Schema(req.body);
        roleEntity.save(function(err, Role) {
			if(err) {
				res.send(err);
			} else {
				res.send(Role)
			}
		});
	}
}
