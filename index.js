'use strict';
var express = require('express'),
    //session = require('express-session'),
    //cookie = require('cookie-parser'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    upload = multer(),
    ejs = require('ejs'),
    app = express(),
    mongoose = require('mongoose');
//Load custom files 
var routes = require('./routes/index'),
    bs = require('./config/bootstrap');

//Connect to database
mongoose.connect('mongodb://localhost:27017/demoApp', function(err) {
    if(err)
        process.exit(1);
    console.log('Connected');
});

app.use(express.static(__dirname + '/public'));
//app.use(cookie());
/*app.use(session({
    secret: 'Site visit',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false, maxAge : 6000000 }
}));*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
//Templating engine load
app.use(upload.array()); 
app.set('view engine', 'html');
app.set('views','./views');
app.engine('html', ejs.renderFile);
// call the routes
app.use(routes);




// call middleware after all routes
/*app.use(function (request, response) {
   response.send("Oops Nothing found");
});*/
// error handler middleware
/*app.use(function (err,request, response, next) {
    response.send("Error Occured : " + err);
});*/
// start up server at the port
app.listen(3000);
