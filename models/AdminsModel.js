var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var address = {
    'street' : String,
    'city' : String,
    'state' : String,
    'country' : String,
    'postCode' : Number,
};
var nominee = {
    'name' : String,
    'mobile' : Number
};
var admin = {
		'firstname': {'type': String, required: [true, 'Why no firstname?']},
		'lastname': {'type': String, lowercase: true},
		'EQ': {'type': String, 'alias': 'educational_qualification'}, //Will create virtual field
		'age': {'type' : Number, 'required': [ageRequire, 'Enter age'], min: [18, 'No child'], max: 100},
		'dob': {'type' : Date, 'default': Date.now},
		'email': {'type': String, validate:[{ validator: isEmail, message: '{VALUE} is not a valid email', isAsync: false}]},
    	'active': {'type' : Boolean, 'default': true},
		'nominee' : nominee,
		'address' : [address],
		'role_id' : [{'type': Schema.Types.ObjectId, 'ref': 'Role'}],
	};

//2nd params are options for schema, not to create id automatically
var adminSchema = new Schema(admin, {/*_id: false,*/ toJSON: {virtuals: true}});
//To include vitual fields, Use .toJSON({ virtuals: true }) return result obj
adminSchema.virtual('nomineeFullName').get(function () {
  return this.nominee.name + ' ' + this.nominee.mobile;
});


module.exports = {
	'Schema': mongoose.model('Admin', adminSchema)
};


//(Dependent Validation)If dob provided then DOB is required
var ageRequire = function() {
    return Date.parse(this.dob) < Date.parse(new Date());
};
function isEmail() {
    var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailRegex.test(this.email);
}