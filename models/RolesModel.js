var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var role = {
    'name' : String,
    'active' : Boolean,
};


var roleSchema = new Schema(role);

module.exports = {
	'Schema': mongoose.model('Role', roleSchema)
};