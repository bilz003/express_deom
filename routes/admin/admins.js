'use strict';
var express = require('express'),
    router = express.Router();

var bs = require('./../../config/bootstrap'),
	adminsController = require(bs.ROOT + '/controllers/admin/AdminsController');   //Admins callback functions

router.get('/list', adminsController.list);
router.post('/add', adminsController.add);
router.get('/find_one', adminsController.findOne);
router.get('/find', adminsController.find);
router.get('/find_id', adminsController.findById);
// router.put('/edit/:id', adminsController.edit);
// router.delete('/delete/:id', adminsController.delete);

module.exports = router;