'use strict';
var express = require('express'),
	app = express();

var adminsPage = require('./admins');
var rolesPage = require('./roles');

app.use('/admins', adminsPage);
app.use('/roles', rolesPage);

module.exports = app;