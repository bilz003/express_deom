'use strict';
var express = require('express'),
    router = express.Router();

var bs = require('./../../config/bootstrap'),
	rolesController = require(bs.ROOT + '/controllers/admin/RolesController');

router.post('/add', rolesController.add);
router.get('/', rolesController.list);

module.exports = router;