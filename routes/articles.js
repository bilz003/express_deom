'use strict';


var express = require('express'),
    router = express.Router(),
	bs = require('./../config/bootstrap');

var articlesController = require(bs.ROOT + '/controllers/ArticlesController');   //Admins callback functions

router.get('/list', articlesController.list);

module.exports = router;