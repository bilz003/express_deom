'use strict';
var express = require('express'),
	app = express();

//For creating prefix route - All call to the admin
var	adminPrefix = require('./admin/index');         //For prefix/different-portal
app.use('/admin', adminPrefix);

var articlesPage = require('./articles');           //For direct pages (No prefix)
app.use('/articles', articlesPage); 

module.exports = app;